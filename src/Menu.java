public class Menu {
    private MobileShop iphone;
    private MobileShop samsung;
    private MobileShop blackberry;

    public Menu(){
        iphone= new Iphone();
        samsung=new Samsung();
        blackberry=new Blackberry();
    }

    public void iphoneSale(){
        iphone.modelo();
        iphone.precio();
    }

    public void samsungSale(){
        samsung.modelo();
        samsung.precio();
    }

    public void blackberrySale(){
        blackberry.modelo();
        blackberry.precio();
    }
}
